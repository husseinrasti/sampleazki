/*
 * Copyright (C) 2022  The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.husseinrasti.build.dependencies

import com.husseinrasti.build.BuildDependenciesVersions


object ThirdParty {

    const val rxJava = "io.reactivex.rxjava3:rxjava:${BuildDependenciesVersions.rxJava}"
    const val rxKotlin = "io.reactivex.rxjava3:rxkotlin:${BuildDependenciesVersions.rxKotlin}"
    const val rxAndroid = "io.reactivex.rxjava3:rxandroid:${BuildDependenciesVersions.rxAndroid}"

    const val rxBinding = "com.jakewharton.rxbinding4:rxbinding:${BuildDependenciesVersions.rxBinding}"
    const val rxBindingRecyclerView = "com.jakewharton.rxbinding2:rxbinding-recyclerview-v7:${BuildDependenciesVersions.rxBinding}"
    const val rxBindingSupportV4 = "com.jakewharton.rxbinding2:rxbinding-support-v4:${BuildDependenciesVersions.rxBinding}"
    const val rxBindingAppcompatV7 = "com.jakewharton.rxbinding2:rxbinding-appcompat-v7:${BuildDependenciesVersions.rxBinding}"
    const val rxBindingDesign = "com.jakewharton.rxbinding2:rxbinding-design:${BuildDependenciesVersions.rxBinding}"
    const val rxBindingLeanBack = "com.jakewharton.rxbinding2:rxbinding-leanback-v17:${BuildDependenciesVersions.rxBinding}"

    // Coroutins
    const val coroutines = "org.jetbrains.kotlinx:kotlinx-coroutines-core:${BuildDependenciesVersions.coroutinesVersion}"
    const val coroutinesAndroid = "org.jetbrains.kotlinx:kotlinx-coroutines-android:${BuildDependenciesVersions.coroutinesAndroid}"
    const val retrofitCoroutines = "com.jakewharton.retrofit:retrofit2-kotlin-coroutines-adapter:${BuildDependenciesVersions.coroutinesRetrofit}"

    const val coil = "io.coil-kt:coil:${BuildDependenciesVersions.coil}"
    const val coilGif = "io.coil-kt:coil-gif:${BuildDependenciesVersions.coil}"
    const val coilSvg = "io.coil-kt:coil-svg:${BuildDependenciesVersions.coil}"

    const val glide = "com.github.bumptech.glide:glide:${BuildDependenciesVersions.glide}"
    const val glideCompiler = "com.github.bumptech.glide:compiler:${BuildDependenciesVersions.glide}"

    const val shimmerEffect = "com.facebook.shimmer:shimmer:0.5.0"

    const val retrofit = "com.squareup.retrofit2:retrofit:${BuildDependenciesVersions.retrofit2Version}"
    const val retrofitRxJavaAdapter = "com.squareup.retrofit2:adapter-rxjava3:${BuildDependenciesVersions.retrofit2RxJava}"
    const val retrofitGson = "com.squareup.retrofit2:converter-gson:${BuildDependenciesVersions.retrofit2Version}"
    const val googleGson = "com.google.code.gson:gson:${BuildDependenciesVersions.gson}"
    const val okHttp = "com.squareup.okhttp3:okhttp:${BuildDependenciesVersions.okHttp}"
    const val okHttpInterceptor = "com.squareup.okhttp3:logging-interceptor:${BuildDependenciesVersions.okHttp}"
}