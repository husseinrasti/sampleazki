package com.husseinrasti.sampleazki

import androidx.multidex.MultiDexApplication
import dagger.hilt.android.HiltAndroidApp


/**
 * Created by Hussein Rasti on 4/17/22.
 */
@HiltAndroidApp
class App : MultiDexApplication()