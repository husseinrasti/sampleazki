package com.husseinrasti.sampleazki.di

import com.husseinrasti.data.ins_prices.remote.InsuranceApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun providePlace(retrofit: Retrofit): InsuranceApi {
        return retrofit.create(InsuranceApi::class.java)
    }

}