package com.husseinrasti.sampleazki.di

import android.app.Application
import android.content.res.Resources
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.husseinrasti.core.network.AuthInterceptor
import com.husseinrasti.core.network.BaseHttpClient
import com.husseinrasti.core.network.BaseRetrofit
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import javax.inject.Singleton


@Module
@InstallIn(SingletonComponent::class)
class AppModule {

    @Provides
    @Singleton
    fun resources(application: Application): Resources {
        return application.resources
    }

    @Provides
    @Singleton
    fun authInterceptor(): Interceptor {
        return AuthInterceptor()
    }

    @Provides
    @Singleton
    fun gson(): Gson {
        return GsonBuilder().create()
    }

    @Provides
    fun okHttpClient(baseHttpClient: BaseHttpClient): OkHttpClient {
        return baseHttpClient.okHttpClient
    }

    @Provides
    fun retrofit(baseRetrofit: BaseRetrofit): Retrofit {
        return baseRetrofit.retrofit
    }

}