package com.husseinrasti.sampleazki.di

import android.content.res.Resources
import com.husseinrasti.data.ins_prices.remote.InsuranceApi
import com.husseinrasti.data.ins_prices.repository.InsuranceRepositoryImpl
import com.husseinrasti.domain.ins_prices.repository.InsuranceRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped


@Module
@InstallIn(ViewModelComponent::class)
class RepositoryModule {

    @Provides
    @ViewModelScoped
    fun providePlaceRepository(
        resources: Resources,
        api: InsuranceApi
    ): InsuranceRepository {
        return InsuranceRepositoryImpl(
            resources = resources,
            api = api
        )
    }

}