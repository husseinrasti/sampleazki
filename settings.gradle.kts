rootProject.name = "SampleAzki"

pluginManagement {
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}

includeBuild("configBuild")

include(
    ":app",
    ":data",
    ":domain",
    ":core"
)
include(":features:ins_comparison")
