package com.husseinrasti.domain.ins_prices.entity

class InsuranceEntity {

    data class Item(
        val id: Long,
        val branchNumber: Int,
        val complaintResponseTime: Double,
        val description: String,
        val discountTitle: String,
        val giftTitle: String,
        val hasGift: Boolean,
        val icon: String,
        val prices: List<Price>,
        val satisfaction: Double,
        val title: String,
        val wealthLevel: Double
    )

    data class Price(
        val coverAmount: Int,
        val coverID: Int,
        val discountedPrice: Int,
        val durationID: Int,
        val durationTitle: String,
        val features: List<Feature>,
        val installmentPrice: Int,
        val penaltyAmount: Int,
        val penaltyDays: Int,
        val price: Int,
        val totalPenalty: Int
    )

    data class Feature(
        val description: String,
        val icon: String,
        val title: String
    )

}