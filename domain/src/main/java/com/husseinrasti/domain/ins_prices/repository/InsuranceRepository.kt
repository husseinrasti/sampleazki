package com.husseinrasti.domain.ins_prices.repository

import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity

interface InsuranceRepository {

    suspend fun getInsPrices(): Result<List<InsuranceEntity.Item>>

}