package com.husseinrasti.domain.ins_prices.usecase

import com.husseinrasti.core.usecase.SuspendUseCase
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity
import com.husseinrasti.domain.ins_prices.repository.InsuranceRepository
import javax.inject.Inject


class GetInsPricesUseCase @Inject constructor(
    private val repository: InsuranceRepository
) : SuspendUseCase<GetInsPricesUseCaseParams, Result<List<InsuranceEntity.Item>>> {
    override suspend fun invoke(params: GetInsPricesUseCaseParams): Result<List<InsuranceEntity.Item>> {
        return repository.getInsPrices()
    }
}


@JvmInline
value class GetInsPricesUseCaseParams(private val nothing: Any)