package com.husseinrasti.core.extensions

import androidx.lifecycle.LifecycleOwner
import com.husseinrasti.core.utils.RxLifecycleHandler
import io.reactivex.rxjava3.core.Flowable


/**
 * Created by Hussein Rasti on 3/22/22.
 */
inline fun <reified T> Flowable<T>.observe(
    owner: LifecycleOwner,
    noinline error: (Throwable) -> Unit,
    noinline observer: (T) -> Unit
) {
    RxLifecycleHandler(owner = owner, flowable = this, error = error, observer = observer)
}