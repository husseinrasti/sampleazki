/*
 * Copyright (C) 2022  The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.husseinrasti.core.extensions

import android.app.Activity
import android.content.Context
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.drawable.Drawable
import android.view.View
import android.view.animation.*
import android.widget.TextView
import androidx.annotation.DrawableRes
import androidx.annotation.StringRes
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.constraintlayout.widget.ConstraintSet
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import androidx.transition.TransitionManager
import com.google.android.material.snackbar.Snackbar
import kotlin.math.cos
import kotlin.math.pow


/**
 * Created by Hussein Rasti on 2/24/22.
 */

fun Activity.drawableCompat(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)
fun Fragment.drawableCompat(@DrawableRes id: Int) = ContextCompat.getDrawable(requireActivity(), id)
fun Context.drawableCompat(@DrawableRes id: Int) = ContextCompat.getDrawable(this, id)

fun TextView.strike() {
    paintFlags = paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
}

fun Drawable.toBitmap(): Bitmap {
    val bitmap: Bitmap = Bitmap.createBitmap(
        intrinsicWidth,
        intrinsicHeight,
        Bitmap.Config.ARGB_8888
    )
    val canvas = Canvas(bitmap)
    setBounds(0, 0, canvas.width, canvas.height)
    draw(canvas)
    return bitmap
}


fun View.visible() {
    visibility = View.VISIBLE
}

fun View.inVisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visibility(show: Boolean, isGone: Boolean = true) {
    if (show) visible() else if (isGone) gone() else inVisible()
}

fun View.notVisibility(show: Boolean, isGone: Boolean = true) {
    if (show) if (isGone) gone() else inVisible() else visible()
}


fun circularProgressDrawable(context: Context): CircularProgressDrawable {
    val circularProgressDrawable = CircularProgressDrawable(context)
    circularProgressDrawable.strokeWidth = 3f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.setColorSchemeColors(Color.RED)
    circularProgressDrawable.start()
    return circularProgressDrawable
}

fun View.blinking() {
    this.startAnimation(AlphaAnimation(1F, 0F).apply {
        duration = 400
        repeatMode = Animation.REVERSE
        repeatCount = Animation.INFINITE
    })
}

fun View.blinking(du: Long = 500, endAlpha: Float = 0F) {
    this.startAnimation(AlphaAnimation(1F, endAlpha).apply {
        duration = du
        repeatMode = Animation.REVERSE
        repeatCount = Animation.INFINITE
    })
}

fun View.bounce() {
    val anim = ScaleAnimation(1.0f, 1.0f, 0.9f, 1.0f)
    this.startAnimation(anim.apply {
        duration = 300
        /*pivotX = 50f
        pivotY = 50f*/
        interpolator = MyBounceInterpolator()
    })
}

class MyBounceInterpolator(val mAmplitude: Double = 1.0, val mFrequency: Double = 10.0) :
    Interpolator {
    override fun getInterpolation(time: Float): Float {
        return (-1 * Math.E.pow(-time / mAmplitude) *
                cos(mFrequency * time) + 1).toFloat()
    }
}

fun View.fadeIn(isAnimation: Boolean = true, time: Long = 300) {
    if (isAnimation) {
        this.startAnimation(AlphaAnimation(0F, 1F).apply {
            duration = time

        })
    }
    this.visibility = View.VISIBLE
}

fun View.fadeOut(isAnimation: Boolean = true) {
    if (isAnimation) {
        this.startAnimation(AlphaAnimation(1F, 0F).apply {
            duration = 300
        })
    }
    this.visibility = View.GONE

}

inline fun View.snack(@StringRes messageRes: Int, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
    snack(resources.getString(messageRes), length, f)
}

inline fun View.snack(message: String, length: Int = Snackbar.LENGTH_LONG, f: Snackbar.() -> Unit) {
    val snack = Snackbar.make(this, message, length)
    snack.f()
    snack.show()
}

fun Snackbar.action(@StringRes actionRes: Int, color: Int? = null, listener: (View) -> Unit) {
    action(view.resources.getString(actionRes), color, listener)
}

fun Snackbar.action(action: String, color: Int? = null, listener: (View) -> Unit) {
    setAction(action, listener)
    color?.let { setActionTextColor(color) }
}

fun ConstraintSet.beginTransition(body: ConstraintLayout) {
    TransitionManager.beginDelayedTransition(body)
    this.applyTo(body)
}