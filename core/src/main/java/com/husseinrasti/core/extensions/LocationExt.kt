package com.husseinrasti.core.extensions

import android.app.Activity
import android.content.Context
import android.content.IntentSender
import android.location.LocationManager
import androidx.fragment.app.Fragment
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.husseinrasti.core.utils.GPS_REQUEST_CODE

fun Fragment.isGps() = try {
    val manager = requireActivity().getSystemService(Context.LOCATION_SERVICE) as LocationManager
    manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
} catch (e: Exception) {
    false
}

fun Activity.isGps() = try {
    val manager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
    manager.isProviderEnabled(LocationManager.GPS_PROVIDER)
} catch (e: Exception) {
    false
}

inline fun Fragment.onEnableLocationDialog() {
    val locationRequest = LocationRequest.create()
    locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
    val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
    builder.setAlwaysShow(true)
    val task: Task<LocationSettingsResponse> = LocationServices.getSettingsClient(requireActivity())
        .checkLocationSettings(builder.build())
    task.addOnCompleteListener {
        try {
            val response: LocationSettingsResponse? = it.getResult(ApiException::class.java)
        } catch (exception: ApiException) {
            when (exception.statusCode) {
                LocationSettingsStatusCodes.RESOLUTION_REQUIRED -> {
                    try {
                        val resolvable: ResolvableApiException = exception as ResolvableApiException
                        startIntentSenderForResult(resolvable.resolution.intentSender, GPS_REQUEST_CODE, null, 0, 0, 0, null)
                    } catch (e: IntentSender.SendIntentException) {
                        e.printStackTrace()
                    } catch (e: ClassCastException) {
                        e.printStackTrace()
                    }
                }
                LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                }
            }
        }
    }
}
