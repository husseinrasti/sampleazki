/*
 * Copyright (C) 2022  The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.husseinrasti.core.extensions

import android.content.Context
import android.graphics.drawable.Drawable
import android.widget.ImageView
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources
import coil.ImageLoader
import coil.imageLoader
import coil.request.Disposable
import coil.request.ImageRequest
import com.husseinrasti.core.R


/**
 * Created by Hussein Rasti on 2/24/22.
 */

inline fun loadImage(
    context: Context,
    data: Any?,
    @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder_24,
    @DrawableRes error: Int = R.drawable.ic_placeholder_24,
    showProgress: Boolean = false,
    imageLoader: ImageLoader = context.imageLoader,
    builder: ImageRequest.Builder.() -> Unit = {},
    crossinline target: (Drawable?) -> Unit
): Disposable {
    val safePlaceholderDrawable = AppCompatResources.getDrawable(context, placeholderRes)
    val safeErrorDrawable = AppCompatResources.getDrawable(context, error)
    val request = ImageRequest.Builder(context)
        .apply(builder)
        .data(data)
        .allowHardware(false)
        .placeholder(
            if (showProgress) circularProgressDrawable(context)
            else safePlaceholderDrawable
        )
        .error(safeErrorDrawable)
        .target(
            onStart = { target(it) },
            onSuccess = { target(it) },
            onError = { target(it) }
        ).build()
    return imageLoader.enqueue(request)
}


inline fun ImageView.load(
    data: Any?,
    @DrawableRes placeholderRes: Int = R.drawable.ic_placeholder_24,
    @DrawableRes error: Int = R.drawable.ic_placeholder_24,
    showProgress: Boolean = false,
    imageLoader: ImageLoader = context.imageLoader,
    builder: ImageRequest.Builder.() -> Unit = {}
): Disposable {
    return loadImage(
        context = context,
        data = data,
        placeholderRes = placeholderRes,
        error = error,
        showProgress = showProgress,
        imageLoader = imageLoader,
        builder = builder
    ) { drawable -> setImageDrawable(drawable) }
}

