package com.husseinrasti.core.utils

open class IrAmount(private val number: Long) {

    companion object {
        private const val separator = ","
    }

    fun digitGrouped(): String {
        return digitGrouped(number.toString())
    }

    private fun digitGrouped(token: String): String {
        val divOfThree = token.length.div(3)
        val remOfThree = token.length.rem(3)

        return when (divOfThree) {
            0 -> token
            1 -> {
                val tail = token.substring(token.length - 3)

                if (remOfThree != 0)
                    token.substring(0, remOfThree) + separator + tail
                else
                    tail
            }
            else -> {
                val head = token.substring(0, token.length - 3)
                val tail = token.substring(token.length - 3)
                var result = tail

                if (head.length > 2) {
                    result = separator + result
                }

                digitGrouped(head) + result
            }
        }
    }
}