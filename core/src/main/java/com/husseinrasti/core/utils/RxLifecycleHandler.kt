package com.husseinrasti.core.utils

import androidx.lifecycle.*
import androidx.lifecycle.Lifecycle.State.DESTROYED
import io.reactivex.rxjava3.core.Flowable
import io.reactivex.rxjava3.disposables.Disposable

class RxLifecycleHandler<T>(
    owner: LifecycleOwner,
    private val flowable: Flowable<T>,
    private val error: (Throwable) -> Unit,
    private val observer: (T) -> Unit
) : LifecycleEventObserver {

    private val lifecycle = owner.lifecycle
    private var disposable: Disposable? = null

    init {
        if (lifecycle.currentState != DESTROYED) {
            owner.lifecycle.addObserver(this)
            observeIfPossible()
        }
    }

    private fun observeIfPossible() {
        if (lifecycle.currentState.isAtLeast(Lifecycle.State.STARTED)) {
            disposable ?: let {
                disposable = flowable.subscribe({ data -> observer(data) }, { error(it) })
            }
        }
    }

    override fun onStateChanged(source: LifecycleOwner, event: Lifecycle.Event) {
        when (event) {
            Lifecycle.Event.ON_START -> {
                observeIfPossible()
            }
            Lifecycle.Event.ON_STOP -> {
                disposable?.dispose()
                disposable = null
            }
            Lifecycle.Event.ON_DESTROY -> {
                lifecycle.removeObserver(this)
            }
        }
    }

}