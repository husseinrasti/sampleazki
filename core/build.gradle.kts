import com.husseinrasti.build.BuildAndroidConfig

plugins {
    id("build-android-library")
}

android {
    buildTypes {
        release {
            buildConfigField("String", "APPLICATION_BASE_ID", "\"" + BuildAndroidConfig.APPLICATION_ID + "\"")
            buildConfigField("String", "SHARED_PREF_NAME", "\"" + BuildAndroidConfig.SHARED_PREF_NAME + "\"")
            buildConfigField("String", "DATABASE_NAME", "\"" + BuildAndroidConfig.DATABASE_NAME + "\"")
            buildConfigField("String", "VERSION_NAME", "\"" + BuildAndroidConfig.VERSION_NAME + "\"")
            buildConfigField("String", "BASE_URL", "\"" + BuildAndroidConfig.BASE_URL + "\"")
        }
        debug {
            buildConfigField("String", "APPLICATION_BASE_ID", "\"" + BuildAndroidConfig.APPLICATION_ID + "\"")
            buildConfigField("String", "SHARED_PREF_NAME", "\"" + BuildAndroidConfig.SHARED_PREF_NAME + "\"")
            buildConfigField("String", "DATABASE_NAME", "\"" + BuildAndroidConfig.DATABASE_NAME + "\"")
            buildConfigField("String", "VERSION_NAME", "\"" + BuildAndroidConfig.VERSION_NAME + "\"")
            buildConfigField("String", "BASE_URL", "\"" + BuildAndroidConfig.BASE_URL + "\"")
        }
    }
}