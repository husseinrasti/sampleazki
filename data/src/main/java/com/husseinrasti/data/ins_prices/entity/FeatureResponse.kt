package com.husseinrasti.data.ins_prices.entity

import com.google.gson.annotations.SerializedName
import com.husseinrasti.core.mapper.ResponseObject
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity

data class FeatureResponse(
    @SerializedName("description")
    val description: String?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("title")
    val title: String?
) : ResponseObject<InsuranceEntity.Feature> {
    override fun toDomain(): InsuranceEntity.Feature {
        return InsuranceEntity.Feature(
            description = description ?: "",
            icon = icon ?: "",
            title = title ?: ""
        )
    }
}