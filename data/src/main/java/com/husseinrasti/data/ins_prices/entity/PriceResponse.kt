package com.husseinrasti.data.ins_prices.entity

import com.google.gson.annotations.SerializedName
import com.husseinrasti.core.mapper.ResponseObject
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity

data class PriceResponse(
    @SerializedName("coverAmount")
    val coverAmount: Int?,
    @SerializedName("coverID")
    val coverID: Int?,
    @SerializedName("discountedPrice")
    val discountedPrice: Int?,
    @SerializedName("durationID")
    val durationID: Int?,
    @SerializedName("durationTitle")
    val durationTitle: String?,
    @SerializedName("features")
    val features: List<FeatureResponse>?,
    @SerializedName("installmentPrice")
    val installmentPrice: Int?,
    @SerializedName("penaltyAmount")
    val penaltyAmount: Int?,
    @SerializedName("penaltyDays")
    val penaltyDays: Int?,
    @SerializedName("price")
    val price: Int?,
    @SerializedName("totalPenalty")
    val totalPenalty: Int?
) : ResponseObject<InsuranceEntity.Price> {
    override fun toDomain(): InsuranceEntity.Price {
        return InsuranceEntity.Price(
            coverAmount = coverAmount ?: 0,
            coverID = coverID ?: 0,
            discountedPrice = discountedPrice ?: 0,
            durationID = durationID ?: 0,
            durationTitle = durationTitle ?: "",
            features = features?.map { it.toDomain() } ?: listOf(),
            installmentPrice = installmentPrice ?: 0,
            penaltyAmount = penaltyAmount ?: 0,
            penaltyDays = penaltyDays ?: 0,
            price = price ?: 0,
            totalPenalty = totalPenalty ?: 0
        )
    }
}
