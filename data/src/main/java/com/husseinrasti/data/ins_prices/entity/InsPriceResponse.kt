package com.husseinrasti.data.ins_prices.entity

import com.google.gson.annotations.SerializedName
import com.husseinrasti.core.mapper.ResponseObject
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity

data class InsPriceResponse(
    @SerializedName("branchNumber")
    val branchNumber: Int?,
    @SerializedName("complaintResponseTime")
    val complaintResponseTime: Double?,
    @SerializedName("description")
    val description: String?,
    @SerializedName("discountTitle")
    val discountTitle: String?,
    @SerializedName("giftTitle")
    val giftTitle: String?,
    @SerializedName("hasGift")
    val hasGift: Boolean?,
    @SerializedName("icon")
    val icon: String?,
    @SerializedName("id")
    val id: Long?,
    @SerializedName("prices")
    val prices: List<PriceResponse>?,
    @SerializedName("satisfaction")
    val satisfaction: Double?,
    @SerializedName("title")
    val title: String?,
    @SerializedName("wealthLevel")
    val wealthLevel: Double?
) : ResponseObject<InsuranceEntity.Item> {
    override fun toDomain(): InsuranceEntity.Item {
        return InsuranceEntity.Item(
            branchNumber = branchNumber ?: 0,
            complaintResponseTime = complaintResponseTime ?: 0.0,
            description = description ?: "",
            title = title ?: "",
            icon = icon ?: "",
            id = id ?: 0,
            discountTitle = discountTitle ?: "",
            satisfaction = satisfaction ?: 0.0,
            wealthLevel = wealthLevel ?: 0.0,
            giftTitle = giftTitle ?: "",
            hasGift = hasGift ?: false,
            prices = prices?.map { it.toDomain() } ?: listOf()
        )
    }
}
