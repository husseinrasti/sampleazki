package com.husseinrasti.data.ins_prices.repository

import android.content.res.Resources
import com.husseinrasti.core.network.fetch
import com.husseinrasti.data.ins_prices.remote.InsuranceApi
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity
import com.husseinrasti.domain.ins_prices.repository.InsuranceRepository
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


class InsuranceRepositoryImpl @Inject constructor(
    private val resources: Resources,
    private val api: InsuranceApi
) : InsuranceRepository {
    override suspend fun getInsPrices(): Result<List<InsuranceEntity.Item>> {
        return fetch(
            dispatcher = Dispatchers.IO,
            resources = resources,
            fetchList = { api.getInsPrices() }
        )
    }
}