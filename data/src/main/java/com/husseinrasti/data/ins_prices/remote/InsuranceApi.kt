package com.husseinrasti.data.ins_prices.remote

import com.husseinrasti.core.network.Urls
import com.husseinrasti.data.ins_prices.entity.InsPriceResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Query


interface InsuranceApi {

    @GET(Urls.InsPrices)
    suspend fun getInsPrices(
        @Header("deviceID") deviceID: Int = 2,
        @Query("marketer") marketer: Boolean = false,
        @Query("zeroKilometer") zeroKilometer: Boolean = false,
        @Query("withoutInsure") withoutInsure: Boolean = false,
        @Query("installment") installment: Boolean = false,
        @Query("oldInsureUsed") oldInsureUsed: Boolean = false,
        @Query("vehicleConstructionYear") vehicleConstructionYear: Int = 1399,
        @Query("vehicleChangedOwner") vehicleChangedOwner: Int = 0,
        @Query("vehicleTypeID") vehicleTypeID: Int = 6,
        @Query("vehicleBrandID") vehicleBrandID: Int = 84,
        @Query("vehicleModelID") vehicleModelID: Int = 806662,
        @Query("oldCompanyID") oldCompanyID: Int = 7,
        @Query("thirdDiscountID") thirdDiscountID: Int = 6,
        @Query("driverDiscountID") driverDiscountID: Int = 6,
        @Query("vehicleUsageID") vehicleUsageID: Int = 1,
        @Query("vehicleColorTitle") vehicleColorTitle: String = "red",
        @Query("oldInsureStartDate") oldInsureStartDate: String = "2020-07-03",
        @Query("oldInsureExpireDate") oldInsureExpireDate: String = "2021-05-20"
    ): Response<List<InsPriceResponse>>

}