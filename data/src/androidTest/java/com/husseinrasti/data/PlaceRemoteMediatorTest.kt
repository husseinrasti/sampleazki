package com.husseinrasti.data

import android.app.Application
import androidx.paging.*
import androidx.paging.RemoteMediator.MediatorResult
import androidx.test.core.app.ApplicationProvider
import androidx.test.ext.junit.runners.AndroidJUnit4
import com.husseinrasti.data.ins_prices.dao.InsuranceDao
import com.husseinrasti.data.ins_prices.datasource.InsDataSource
import com.husseinrasti.data.ins_prices.datasource.InsRemoteMediator
import com.husseinrasti.data.ins_prices.remote.InsuranceApi
import com.husseinrasti.data.remoteKeys.dao.RemoteKeysDao
import com.husseinrasti.data.remoteKeys.datasource.RemoteKeysDataSource
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.test.runTest
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import kotlin.test.assertFalse
import kotlin.test.assertTrue


/**
 * Created by Hussein Rasti on 3/22/22.
 */
@ExperimentalPagingApi
@OptIn(ExperimentalCoroutinesApi::class)
@RunWith(AndroidJUnit4::class)
class PlaceRemoteMediatorTest {

    lateinit var insRemoteMediator: InsRemoteMediator

    @Mock
    private lateinit var api: InsuranceApi

    @Mock
    private lateinit var placeDao: InsuranceDao
    private lateinit var insDataSource: InsDataSource

    @Mock
    private lateinit var remoteKeysDao: RemoteKeysDao
    private lateinit var remoteKeysDataSource: RemoteKeysDataSource

    @Before
    fun setup() {
        insDataSource = InsDataSource(placeDao)
        remoteKeysDataSource = RemoteKeysDataSource(remoteKeysDao)
        insRemoteMediator = InsRemoteMediator(
            resources = ApplicationProvider.getApplicationContext<Application>().resources,
            api = api,
            insDataSource = insDataSource,
            remoteKeysDataSource = remoteKeysDataSource
        )
    }

    @Test
    fun refreshLoadReturnsSuccessResultWhenMoreDataIsPresent() = runTest {
        val pagingState = PagingState<Int, InsuranceEntity.Item>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = insRemoteMediator.load(LoadType.REFRESH, pagingState)
        assertTrue { result is MediatorResult.Success }
        assertFalse { (result as MediatorResult.Success).endOfPaginationReached }
    }

    @Test
    fun refreshLoadSuccessAndEndOfPaginationWhenNoMoreData() = runTest {
        val pagingState = PagingState<Int, InsuranceEntity.Item>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = insRemoteMediator.load(LoadType.REFRESH, pagingState)
        assertTrue { result is MediatorResult.Success }
        assertTrue { (result as MediatorResult.Success).endOfPaginationReached }
    }

    @Test
    fun refreshLoadReturnsErrorResultWhenErrorOccurs() = runTest {
        val pagingState = PagingState<Int, InsuranceEntity.Item>(
            listOf(),
            null,
            PagingConfig(10),
            10
        )
        val result = insRemoteMediator.load(LoadType.REFRESH, pagingState)
        assertTrue { result is MediatorResult.Error }
    }

}