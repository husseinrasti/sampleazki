import com.husseinrasti.build.BuildModules

plugins {
    id("build-android-library")
}

dependencies {
    implementation(project(BuildModules.CORE))
    implementation(project(BuildModules.DOMAIN))
}