package com.husseinrasti.ins_comparison

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.husseinrasti.core.extensions.load
import com.husseinrasti.core.extensions.strike
import com.husseinrasti.core.utils.IrAmount
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity
import com.husseinrasti.ins_comparison.databinding.AdapterItemInsBinding
import kotlin.random.Random


/**
 * Created by Hussein Rasti on 4/18/22.
 */
class InsuranceAdapter : ListAdapter<InsuranceEntity.Item, InsuranceAdapter.ViewHolder>(DiffCallBack()) {

    private var context: Context? = null

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            AdapterItemInsBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }


    inner class ViewHolder(
        private val binding: AdapterItemInsBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        @SuppressLint("StringFormatMatches")
        fun bind(item: InsuranceEntity.Item) {
            binding.name.text = item.title
            binding.logo.load(item.icon)
            val price = item.prices.first().price.toLong()
            binding.price.text = IrAmount(price).digitGrouped()
            binding.discountPrice.text = IrAmount(price - ((price / 100) * 10)).digitGrouped()
            binding.discountPrice.strike()
            binding.rate.text = String.format("%.1f", Random.nextDouble(1.0, 5.0))
            binding.rateCount.text = context?.getString(R.string.title_rate, (50..1000).random())
            binding.root.setOnClickListener { }
        }
    }

    private class DiffCallBack : DiffUtil.ItemCallback<InsuranceEntity.Item>() {
        override fun areItemsTheSame(oldItem: InsuranceEntity.Item, newItem: InsuranceEntity.Item): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: InsuranceEntity.Item, newItem: InsuranceEntity.Item): Boolean {
            return oldItem == newItem
        }
    }

}