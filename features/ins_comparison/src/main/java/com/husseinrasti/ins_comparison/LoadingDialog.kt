package com.husseinrasti.ins_comparison

import android.app.Dialog
import android.graphics.drawable.InsetDrawable
import android.graphics.drawable.NinePatchDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager
import com.husseinrasti.core.extensions.tryCatch

class LoadingDialog : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false

        // Remove BG of the Dialog
        dialog?.window?.decorView?.background?.let {
            if (it is InsetDrawable) {
                it.alpha = 0
            } else if (it is NinePatchDrawable) {
                it.alpha = 0
            }
        }

        return inflater.inflate(R.layout.loading_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        tryCatch { dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE) }
        return dialog
    }

    override fun onResume() {
        super.onResume()
        tryCatch {
            dialog!!.window!!.setLayout(
                resources.getDimensionPixelSize(R.dimen._90dp),
                resources.getDimensionPixelSize(R.dimen._90dp)
            )
        }
    }

    fun show(manager: FragmentManager) {
        super.show(manager, javaClass.simpleName)
    }

}