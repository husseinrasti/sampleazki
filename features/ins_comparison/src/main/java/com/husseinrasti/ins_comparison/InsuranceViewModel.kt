package com.husseinrasti.ins_comparison

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.husseinrasti.core.utils.Logger
import com.husseinrasti.domain.ins_prices.entity.InsuranceEntity
import com.husseinrasti.domain.ins_prices.usecase.GetInsPricesUseCase
import com.husseinrasti.domain.ins_prices.usecase.GetInsPricesUseCaseParams
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.flow.consumeAsFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Hussein Rasti on 4/18/22.
 */
@HiltViewModel
class InsuranceViewModel @Inject constructor(
    private val getInsPricesUseCase: GetInsPricesUseCase
) : ViewModel() {

    val userIntent = Channel<InsuranceIntent>(Channel.UNLIMITED)

    private val _uiState = MutableStateFlow<InsuranceUiState>(InsuranceUiState.Idle)
    val uiState = _uiState.asStateFlow()

    init {
        handleIntent()
    }

    private fun handleIntent() {
        viewModelScope.launch {
            userIntent.consumeAsFlow().collect { intent ->
                when (intent) {
                    is InsuranceIntent.GetInsurance -> getInsurance()
                }
            }
        }
    }

    private fun getInsurance() {
        viewModelScope.launch {
            getInsPricesUseCase.invoke(GetInsPricesUseCaseParams(""))
                .fold(
                    onFailure = {
                        Logger.e("onFailure: $it , ${it.message}")
                        _uiState.emit(InsuranceUiState.Error(it.message))
                    },
                    onSuccess = {
                        Logger.e("onSuccess: $it")
                        _uiState.emit(InsuranceUiState.Insurance(it))
                    }
                )
        }
    }

}


sealed class InsuranceIntent {
    object GetInsurance : InsuranceIntent()
}


sealed class InsuranceUiState {
    data class Insurance(val data: List<InsuranceEntity.Item>) : InsuranceUiState()
    data class Error(val msg: String?) : InsuranceUiState()
    object Idle : InsuranceUiState()
}
