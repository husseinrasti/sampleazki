package com.husseinrasti.ins_comparison

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.husseinrasti.core.extensions.tryCatch
import com.husseinrasti.ins_comparison.databinding.FragmentInsuranceBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest


/**
 * Created by Hussein Rasti on 4/18/22.
 */
@AndroidEntryPoint
class InsuranceFragment : Fragment() {

    private val viewModel: InsuranceViewModel by viewModels()
    private val adapter: InsuranceAdapter by lazy { InsuranceAdapter() }
    private var loadingDialog: LoadingDialog? = null

    private var _binding: FragmentInsuranceBinding? = null
    private val binding: FragmentInsuranceBinding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        lifecycleScope.launchWhenStarted {
            viewModel.userIntent.send(InsuranceIntent.GetInsurance)
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collectLatest {
                    when (it) {
                        InsuranceUiState.Idle -> {
                            onShowLoading()
                        }
                        is InsuranceUiState.Error -> {
                            onHideLoading()
                            Toast.makeText(requireContext(), it.msg, Toast.LENGTH_SHORT).show()
                        }
                        is InsuranceUiState.Insurance -> {
                            onHideLoading()
                            adapter.submitList(it.data)
                        }
                    }
                }
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        _binding = FragmentInsuranceBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = adapter
    }

    private fun onShowLoading() {
        tryCatch {
            parentFragmentManager.let parentFragmentManager@{ fragmentManager ->
                loadingDialog?.let { return@parentFragmentManager }
                loadingDialog = LoadingDialog().also {
                    if (it.dialog != null && it.dialog!!.isShowing && !it.isRemoving) return@also
                    it.show(fragmentManager)
                }
            }
        }
    }

    private fun onHideLoading() {
        tryCatch {
            loadingDialog?.let {
                it.dismiss()
                loadingDialog = null
            }
        }
    }

}